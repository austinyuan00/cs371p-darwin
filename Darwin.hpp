// ---------
// Darwin.hpp
// ---------

#ifndef Darwin_h
#define Darwin_h

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair
#include <vector>

#include "Species.hpp"
#include "Creature.hpp"
#include <climits>
#include <cstdlib>
using namespace std;

enum Direction {NORTH, EAST, SOUTH, WEST};
enum Command {HOP, LEFT, RIGHT, INFECT, IF_EMPTY, IF_WALL, IF_RANDOM, IF_ENEMY, GO};
class Creature;
class Darwin;
class Species;

class Darwin {
private:
    vector<Creature> creatures;
    int rows;
    int columns;
    vector<vector<Creature*>> board;

public:
    // creates the board with r rows and c columns, initializes vector of creatures
    Darwin(int r, int c, int numCreatures);

    //destructor
    // ~Darwin();

    //move one round of grid
    void round();

    //adds a creature to the board
    bool add(Species& s, int direction, int r, int c);

    // prints the current state of the board
    void show(bool isLast);

    bool hop(int& r, int& c, int direction);

    bool left(int& r, int& c, int direction);

    bool right(int& r, int& c, int direction);

    bool infect(int& r, int& c, int direction);

    bool if_empty(int r, int c, int direction);

    bool if_wall(int& r, int& c, int direction);

    bool if_random(int& r, int& c, int direction);

    bool if_enemy(int r, int c, int direction);

    bool go(int& r, int& c, int direction);
};


#endif //Darwin_h
