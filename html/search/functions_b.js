var searchData=
[
  ['set_5fpc',['set_pc',['../classCreature.html#a0c7ad6b8eddc5a41c90864038667afa5',1,'Creature::set_pc(int x)'],['../classCreature.html#a0c7ad6b8eddc5a41c90864038667afa5',1,'Creature::set_pc(int x)']]],
  ['set_5fturn',['set_turn',['../classCreature.html#a25d626827760bf488fb2a96e93dc5690',1,'Creature::set_turn(bool end)'],['../classCreature.html#a25d626827760bf488fb2a96e93dc5690',1,'Creature::set_turn(bool end)']]],
  ['show',['show',['../classDarwin.html#ab75f524202f618837e4bee4a9b8744a0',1,'Darwin::show(bool isLast)'],['../classDarwin.html#ab75f524202f618837e4bee4a9b8744a0',1,'Darwin::show(bool isLast)']]],
  ['species',['Species',['../classSpecies.html#a97b269ca8811d963a95329cdbabfb97c',1,'Species::Species(char t)'],['../classSpecies.html#a97b269ca8811d963a95329cdbabfb97c',1,'Species::Species(char t)']]],
  ['start',['start',['../classCreature.html#a5912ead54083d3d660ab1a371509d477',1,'Creature::start(Darwin &amp;d)'],['../classCreature.html#a5912ead54083d3d660ab1a371509d477',1,'Creature::start(Darwin &amp;d)']]]
];
