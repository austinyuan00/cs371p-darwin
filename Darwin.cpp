// -----------
// Darwin.cpp
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Darwin.hpp"

#include <climits>
#include <cstdlib>
using namespace std;

//replace all -1 addresses with nullptr
Darwin::Darwin(int r, int c, int numCreatures) {
    rows = r;
    columns = c;
    creatures.reserve(numCreatures);
    board.resize(r, vector<Creature*>(c, nullptr));
}

// Darwin::~Darwin() {
//     for(int i = 0; i < row; i++) {
//         board[i].clear();
//     }
//     board.clear();
//     creatures.clear();
// }

void Darwin::round() {
    for(int r = 0; r < rows; r++) {
        for(int c = 0; c < columns; c++) {
            //check if address is valid
            if(board[r][c] != nullptr) {
                // board is just addresses? can dereference (i might need to implement the * operator??)
                Creature& current = *board[r][c];
                if(!current.is_done())
                    current.start(*this);
            }
        }
    }
    // make sure to undo the end turn flag on every creature
    for(int i = 0; i < creatures.size(); i++) {
        creatures[i].set_turn(false);
    }
}

//returns whether or not a creature was successfully added
bool Darwin::add(Species& s, int direction, int r, int c) {
    Creature current(s, direction, r, c);
    creatures.push_back(current);
    board[r][c] = &creatures.back(); //should i use &(creatures[creatures.size() - 1]) instead?
    return true;
}

void Darwin::show(bool isLast) {
    cout << "  ";
    for(int c = 0; c < columns; c++)
        cout << c%10;
    cout << "\n";
    cout.flush();
    for(int r = 0; r < rows; r++) {
        cout << r%10 << " ";
        for(int c = 0; c < columns; c++) {
            if(board[r][c] != nullptr) {
                board[r][c]->print();
            }
            else
                cout << ".";
        }
        cout << "\n";
    }
    if(!isLast)
        cout << "\n";
}

bool Darwin::hop(int& r, int& c, int direction) {
    assert(board[r][c] != nullptr);
    if(if_empty(r, c, direction)) {
        switch(direction) {
        case NORTH:
            board[r-1][c] = board[r][c];
            board[r--][c] = nullptr;
            break;
        case EAST:
            board[r][c+1] = board[r][c];
            board[r][c++] = nullptr;
            break;
        case SOUTH:
            board[r+1][c] = board[r][c];
            board[r++][c] = nullptr;
            break;
        case WEST:
            board[r][c-1] = board[r][c];
            board[r][c--]= nullptr;
            break;
        }
        return true;
    }
    return false;
}

bool Darwin::left(int& r, int& c, int direction) {
    assert(board[r][c] != nullptr);
    //derefrencing creature from address given by board
    (*board[r][c]).turn_left();
    return true;
}

bool Darwin::right(int& r, int& c, int direction) {
    assert(board[r][c] != nullptr);
    //derefrencing creature from address given by board
    (*board[r][c]).turn_right();
    return true;
}

bool Darwin::infect(int& r, int& c, int direction) {
    if(if_enemy(r,c,direction)) {
        //get the creature being infected
        int otherR = r;
        int otherC = c;
        switch(direction) {
        case NORTH:
            otherR--;
            break;
        case EAST:
            otherC++;
            break;
        case SOUTH:
            otherR++;
            break;
        case WEST:
            otherC--;
            break;
        }
        (*board[otherR][otherC]).mySpecies = (*board[r][c]).mySpecies;
        (*board[otherR][otherC]).set_pc(0);
        return true;
    }
    return false;
}

bool Darwin::if_empty(int r, int c, int direction) {
    assert(board[r][c] != nullptr);
    if(!if_wall(r, c, direction)) {
        switch(direction) {
        case NORTH:
            return board[r-1][c] == nullptr;
        case EAST:
            return board[r][c+1] == nullptr;
        case SOUTH:
            return board[r+1][c] == nullptr;
        case WEST:
            return board[r][c-1] == nullptr;
        }
    }
    //wall means not empty
    return false;

}

bool Darwin::if_wall(int& r, int& c, int direction) {
    assert(board[r][c] != nullptr);
    switch(direction) {
    case NORTH:
        return r == 0;
    case EAST:
        return c == columns - 1;
    case SOUTH:
        return r == rows - 1;
    case WEST:
        return c == 0;
    }
    // should be unreachable
    return false;
}

bool Darwin::if_random(int& r, int& c, int direction) {
    //even = fail, odd = success
    if(rand() % 2 == 0)
        return false;
    return true;
}

bool Darwin::if_enemy(int r, int c, int direction) {
    if(board[r][c] == nullptr || if_wall(r,c,direction) || if_empty(r,c,direction))
        return false;
    // now has to be a creature
    int otherR = r;
    int otherC = c;
    switch(direction) {
    case NORTH:
        otherR--;
        break;
    case EAST:
        otherC++;
        break;
    case SOUTH:
        otherR++;
        break;;
    case WEST:
        otherC--;
        break;
    }
    return (*board[r][c]).check_enemy(*board[otherR][otherC]);
}

//always jump to given line
bool Darwin::go(int& r, int& c, int direction) {
    return true;
}
