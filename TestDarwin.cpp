
// ---------------
// TestDarwin.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iterator> // istream_iterator
#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair
#include <iostream>
#include <string>

#include "gtest/gtest.h"

#include "Darwin.hpp"
#include "Species.hpp"
#include "Creature.hpp"


class Darwin;
class Species;
class Creature;

using namespace std;

// ----
// constructors
// ----

TEST(Constructors, InstructionConstructor1) {
    Instruction i(INFECT);
    ASSERT_EQ(i.type, 1);
}

TEST(Constructors, InstructionConstructor2) {
    Instruction i(GO, 1);
    ASSERT_EQ(i.type, 0);
}

TEST(Constructors, InstructionConstructor3) {
    Instruction i(LEFT);
    ASSERT_EQ(i.type, 1);
}

TEST(Constructors, InstructionConstructor4) {
    Instruction i(IF_WALL, 3);
    ASSERT_EQ(i.type, 0);
}

TEST(Constructors, InstructionConstructor5) {
    Instruction i(GO, 200);
    ASSERT_EQ(i.jump, 200);
}

TEST(Constructors, InstructionConstructor6) {
    Instruction i(LEFT);
    ASSERT_EQ(i.type, 1);
    ASSERT_EQ(i.jump, -1);
}

TEST(Constructors, InstructionConstructor7) {
    Instruction i(GO, 15);
    ASSERT_EQ(i.type, 0);
    ASSERT_EQ(i.jump, 15);
}

TEST(Constructors, SpeciesConstructor1) {
    Species alien('a');
    ASSERT_EQ(alien.type, 'a');
}

TEST(Constructors, SpeciesConstructor2) {
    Species saiyan('s');
    ASSERT_EQ(saiyan.type, 's');
}

TEST(Constructors, SpeciesConstructor3) {
    Species pokemon('p');
    ASSERT_EQ(pokemon.type, 'p');
}

TEST(Constructors, SpeciesConstructor4) {
    Species squirtle('s');
    ASSERT_EQ(squirtle.type, 's');
}

TEST(Constructors, SpeciesConstructor5) {
    Species anime('a');
    ASSERT_EQ(anime.type, 'a');
}

TEST(Constructors, CreatureConstructor1) {
    Species rover('r');
    Creature mars(rover, SOUTH, 0, 0);
    ASSERT_EQ(mars.mySpecies, &rover);
}

TEST(Constructors, CreatureConstructor2) {
    Species trap('t');
    Creature card(trap, SOUTH, 0, 0);
    ASSERT_EQ(card.mySpecies, &trap);
}

TEST(Constructors, CreatureConstructor3) {
    Species food('f');
    Creature pizza(food, SOUTH, 0, 0);
    ASSERT_EQ(pizza.mySpecies, &food);
}
TEST(Constructors, CreatureConstructor4) {
    Species hopper('h');
    Creature bunny(hopper, SOUTH, 0, 0);
    ASSERT_EQ(bunny.mySpecies, &hopper);
}

TEST(Constructors, CreatureConstructor5) {
    Species hopper('h');
    Creature bunny(hopper, SOUTH, 0, 0);
    ASSERT_EQ(bunny.is_done(), false);
}
//17
TEST(Constructors, CreatureConstructor6) {
    Species dinosaur('d');
    Creature trex(dinosaur, SOUTH, 0, 0);
    ASSERT_EQ(trex.mySpecies, &dinosaur);
}

TEST(Constructors, CreatureConstructor7) {
    Species lcs('l');
    Creature tsm(lcs, SOUTH, 0, 0);
    ASSERT_EQ(tsm.mySpecies, &lcs);
}

TEST(Constructors, CreatureConstructor8) {
    Species pro('p');
    Creature faker(pro, SOUTH, 0, 0);
    ASSERT_EQ(faker.mySpecies, &pro);
}
