// ---------
// Creature.hpp
// ---------

#ifndef Creature_h
#define Creature_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <string>   // string
#include <utility>  // pair
#include <vector>    // push_back()?
#include <cassert>

#include "Darwin.hpp"
#include "Species.hpp"

using namespace std;

class Species;
class Darwin;

class Creature {
private:
    int direction;
    int row;
    int col;
    int programCounter;
    bool turnEnded;

public:
    Species* mySpecies;

    Creature(Species& s, int dir, int r, int c);

    //start running instructions
    void start(Darwin& d);
    //change direction 90 degrees left
    void turn_left();
    //change direction 90 degrees right
    void turn_right();
    //prints first character of species, calls species.print()
    void print();
    //checks if other is a different species
    bool check_enemy(Creature& other);
    //sets the turnEnded flag
    void set_turn(bool end);
    //sets this creatures programCounter
    void set_pc(int x);
    //checks for ended turn
    bool is_done();
};

#endif //Creature_h
