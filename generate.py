import random
import os
import filecmp

types = 'fhrt'
dirs = 'nesw'
count = 0
while True:
    print(count)
    count += 1
    with open('test.in', 'w') as f:
        f.write('1\n\n')
        f.write('30 30\n')
        num_creatures = random.randint(1,200)
        f.write(str(num_creatures) + '\n')
        s = set()
        i = 0
        while i < num_creatures:
            x = random.randint(0,29)
            y = random.randint(0,29)
            if (x,y) in s:
                continue
            s.add((x,y))
            cur_type = types[random.randint(0,3)]
            dir = dirs[random.randint(0,3)]
            f.write(f'{cur_type} {x} {y} {dir}\n')
            i += 1
        f.write('30 1\n')
    os.system('./Kevin < test.in > kevin.out')
    os.system('./HackerRank < test.in > temp.out')
    if not filecmp.cmp('kevin.out', 'temp.out'):
        break
