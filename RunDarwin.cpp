// --------------
// RunDarwin.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Darwin.hpp"
#include "Creature.hpp"
#include "Species.hpp"

using namespace std;

// ----
// main
// ----

class Darwin;
class Species;

int main () {
    Species food('f');
    food.add_action(LEFT);
    food.add_control(GO, 0);

    Species hopper('h');
    hopper.add_action(HOP);
    hopper.add_control(GO, 0);

    Species rover('r');
    rover.add_control(IF_ENEMY,9);
    rover.add_control(IF_EMPTY,7);
    rover.add_control(IF_RANDOM,5);
    rover.add_action(LEFT);
    rover.add_control(GO,0);
    rover.add_action(RIGHT);
    rover.add_control(GO,0);
    rover.add_action(HOP);
    rover.add_control(GO,0);
    rover.add_action(INFECT);
    rover.add_control(GO,0);

    Species trap('t');
    trap.add_control(IF_ENEMY,3);
    trap.add_action(LEFT);
    trap.add_control(GO,0);
    trap.add_action(INFECT);
    trap.add_control(GO,0);

    int testCases;
    cin >> testCases;
    assert(testCases > 0);
    while(testCases-- > 0) {
        srand(0);
        string flush;
        getline(cin, flush);
        int rows;
        int columns;
        cin >> rows;
        cin >> columns;
        //print *** Darwin r x c ***
        cout << "*** Darwin " << rows << "x" << columns << " ***\n";


        int numCreatures;
        cin >> numCreatures;
        Darwin d(rows, columns, numCreatures);
        while(numCreatures-- > 0) {
            char name;
            int r;
            int c;
            char direction;
            cin >> name;
            cin >> r;
            cin >> c;
            cin >> direction;
            int dir;
            switch(direction) {
            case 'n':
                dir = 0;
                break;
            case 'e':
                dir = 1;
                break;
            case 's':
                dir = 2;
                break;
            case 'w':
                dir = 3;
                break;
            }
            switch(name) {
            case 'f':
                d.add(food, dir, r, c);
                break;
            case 'h':
                d.add(hopper, dir, r, c);
                break;
            case 'r':
                d.add(rover, dir, r, c);
                break;
            case 't':
                d.add(trap, dir, r, c);
                break;
            }
        }
        int turns;
        int freq;
        cin >> turns;
        cin >> freq;
        for(int t = 0; t <= turns; t++) {
            if(t % freq == 0) {
                cout << "Turn = " << t << "." << "\n";
                bool isLast = (testCases == 0) && (t == (turns/freq * freq));
                d.show(isLast);
            }
            if(t != turns)
                d.round();
        }
    }
    return 0;
}
