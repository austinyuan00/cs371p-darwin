.DEFAULT_GOAL := all
MAKEFLAGS     += --no-builtin-rules
SHELL         := bash

ASTYLE        := astyle
CHECKTESTDATA := checktestdata
CPPCHECK      := cppcheck
DOXYGEN       := doxygen
VALGRIND      := valgrind

ifeq ($(shell uname -s), Darwin)
    BOOST    := /usr/local/include/boost
    CXX      := g++-10
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -I/usr/local/include -Wall -Wextra
    GCOV     := gcov-10
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main
else ifeq ($(shell uname -p), unknown)
    BOOST    := /usr/include/boost
    CXX      := g++
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov
    GTEST    := /usr/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
else
    BOOST    := /usr/include/boost
    CXX      := g++-9
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov-9
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
endif

# run docker
docker:
	docker run -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

# get git config
config:
	git config -l

# get git log
Darwin.log:
	git log > Darwin.log

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the Darwin code repo
pull:
	make clean
	@echo
	git pull
	git status

# upload files to the Darwin code repo
push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Darwin.hpp
	-git add Darwin.log
	-git add html
	git add makefile
	git add README.md
	git add RunDarwin.cpp
	git add RunDarwin.ctd
	git add RunDarwin.in
	git add RunDarwin.out
	git add TestDarwin.cpp
	git commit -m "another commit"
	git push
	git status

# compile run harness
RunDarwin: RunDarwin.cpp Darwin.cpp Creature.cpp Species.cpp
	-$(CPPCHECK) RunDarwin.cpp
	$(CXX) $(CXXFLAGS) RunDarwin.cpp Darwin.cpp Creature.cpp Species.cpp -o RunDarwin

# compile test harness
TestDarwin: TestDarwin.cpp Darwin.cpp Creature.cpp Species.cpp
	-$(CPPCHECK) TestDarwin.cpp
	$(CXX) $(CXXFLAGS) TestDarwin.cpp Darwin.cpp Creature.cpp Species.cpp -o TestDarwin $(LDFLAGS)

# run/test files, compile with make all
FILES :=                                  \
    RunDarwin                            \
    TestDarwin

# compile all
all: $(FILES)

# check integrity of input file
ctd-check:
	-$(CHECKTESTDATA) RunDarwin.ctd RunDarwin.in

# generate a random input file
ctd-generate:
	$(CHECKTESTDATA) -g RunDarwin.ctd RunDarwin.tmp

# execute run harness and diff with expected output
run: ctd-check RunDarwin
	./RunDarwin < RunDarwin.in > RunDarwin.tmp
	-diff RunDarwin.tmp RunDarwin.out

# execute test harness
test: TestDarwin
	$(VALGRIND) ./TestDarwin
	$(GCOV) TestDarwin.cpp | grep -B 2 "cpp.gcov"

# clone the Darwin test repo
darwin-tests:
	git clone https://gitlab.com/gpdowning/cs371p-darwin-tests.git darwin-tests

# test files in the Darwin test repo
TFILES := `ls darwin-tests/*.in`

# execute run harness against a test in Darwin test repo and diff with expected output
darwin-tests/%: RunDarwin
	./RunDarwin < $@.in > RunDarwin.tmp
	-diff RunDarwin.tmp $@.out

# execute run harness against all tests in Darwin test repo and diff with expected output
tests: darwin-tests RunDarwin
	-for v in $(TFILES); do make $${v/.in/}; done

# auto format the code
format:
	$(ASTYLE) Darwin.hpp
	$(ASTYLE) RunDarwin.cpp
	$(ASTYLE) TestDarwin.cpp
	$(ASTYLE) Darwin.cpp
	$(ASTYLE) Creature.cpp
	$(ASTYLE) Species.cpp
	$(ASTYLE) Creature.hpp
	$(ASTYLE) Species.hpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile Darwin.hpp
	$(DOXYGEN) Doxyfile

# check files, check their existence with make check
CFILES :=          \
    .gitignore     \
    .gitlab-ci.yml \
    Darwin.log  \
    html

# check the existence of check files
check: $(CFILES)

# remove executables and temporary files
clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunDarwin
	rm -f TestDarwin

# remove executables, temporary files, and generated files
scrub:
	make clean
	rm -f  Darwin.log
	rm -f  Doxyfile
	rm -rf darwin-tests
	rm -rf html
	rm -rf latex

# output versions of all tools
versions:
	@echo "% shell uname -p"
	@echo  $(shell uname -p)
	@echo
	@echo "% shell uname -s"
	@echo  $(shell uname -s)
	@echo
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version
	@echo
	@echo "% grep \"#define BOOST_LIB_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_LIB_VERSION " $(BOOST)/version.hpp
	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version
	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version
	@echo
	@$(CXX) --version
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version
	@echo
	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version
	@echo
	@echo "% cat $(GTEST)/README"
	@cat $(GTEST)/README
ifneq ($(shell uname -s), Darwin)
	@echo
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif
