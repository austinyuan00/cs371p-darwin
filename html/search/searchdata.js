var indexSectionsWithContent =
{
  0: "abcdeghijlmnprstwxy",
  1: "cdis",
  2: "g",
  3: "cdghrst",
  4: "acdeghilmprst",
  5: "bcdijmnprstxy",
  6: "cd",
  7: "eghilnrsw",
  8: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Pages"
};

