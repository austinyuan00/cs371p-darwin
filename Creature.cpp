// -----------
// Creature.cpp
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Creature.hpp"

#include <climits>
using namespace std;

Creature::Creature(Species& s, int dir, int r, int c) {
    mySpecies = &s;
    direction = dir;
    row = r;
    col = c;
    programCounter = 0;
    turnEnded = false;
}

void Creature::start(Darwin& d) {
    mySpecies->execute_instructions(d, row, col, direction, programCounter);
    turnEnded = true;
}

void Creature::turn_left() {
    switch(direction) {
    case NORTH:
        direction = WEST;
        break;
    case EAST:
        direction = NORTH;
        break;
    case SOUTH:
        direction = EAST;
        break;
    case WEST:
        direction = SOUTH;
        break;
    }
}

void Creature::turn_right() {
    switch(direction) {
    case NORTH:
        direction = EAST;
        break;
    case EAST:
        direction = SOUTH;
        break;
    case SOUTH:
        direction = WEST;
        break;
    case WEST:
        direction = NORTH;
        break;
    }
}

void Creature::print() {
    mySpecies->print();
}

bool Creature::check_enemy(Creature& other) {
    //comparing pointers is ok because there is only 1 instance of each species
    return mySpecies != other.mySpecies;
}

void Creature::set_turn(bool end) {
    turnEnded = end;
}

void Creature::set_pc(int x) {
    programCounter = x;
}

bool Creature::is_done() {
    return turnEnded;
}
