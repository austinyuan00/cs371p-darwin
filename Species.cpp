// -----------
// Species.cpp
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Species.hpp"

#include <climits>
using namespace std;

Species::Species(char t) {
    type = t;
}

void Species::execute_instructions(Darwin& d, int& r, int& c, int dir, int& pc) {
    while(pc < iList.size()) {
        Instruction& current = iList[pc];
        // action = 1 : 1 == true?
        if(current.type) {
            // call darwin's execute instruction
            switch(current.getName()) {
            case HOP:
                d.hop(r, c, dir);
                break;
            case LEFT:
                d.left(r, c, dir);
                break;
            case RIGHT:
                d.right(r, c, dir);
                break;
            case INFECT:
                d.infect(r, c, dir);
                break;
            }
            pc++;
            return;
        }
        //control instruction
        else {
            bool success = false;
            switch(current.getName()) {
            case IF_EMPTY:
                success = d.if_empty(r, c, dir);
                break;
            case IF_WALL:
                success = d.if_wall(r, c, dir);
                break;
            case IF_RANDOM:
                success = d.if_random(r, c, dir);
                break;
            case IF_ENEMY:
                success = d.if_enemy(r, c, dir);
                break;
            case GO:
                success = d.go(r, c, dir);
                break;
            }
            if(success)
                pc = current.jump;
            else
                pc++;
        }
    }
}

void Species::add_action(int i) {
    Instruction current(i);
    iList.push_back(current);
}

void Species::add_control(int i, int n) {
    Instruction current(i, n);
    iList.push_back(current);
}

void Species::print() {
    cout << type;
}

Instruction::Instruction(int i) {
    name = i;
    //1 = action
    type = 1;
    //this should never be used
    jump = -1;
}

Instruction::Instruction(int i, int n) {
    name = i;
    //0 = control
    type = 0;
    jump = n;
}

int Instruction::getName() {
    return name;
}
