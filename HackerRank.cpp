// -----------
// Darwin.cpp
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair
#include <vector>

#include <climits>
#include <cstdlib>
using namespace std;

enum Direction {NORTH, EAST, SOUTH, WEST};
enum Command {HOP, LEFT, RIGHT, INFECT, IF_EMPTY, IF_WALL, IF_RANDOM, IF_ENEMY, GO};
class Creature;
class Darwin;
class Species;

class Darwin {
    private:
        vector<Creature> creatures;
        int rows;
        int columns;
        vector<vector<Creature*>> board;

    public:
        // creates the board with r rows and c columns, initializes vector of creatures
        Darwin(int r, int c, int numCreatures);

        //destructor
        // ~Darwin();

        //move one round of grid
        void round();

        //adds a creature to the board
        bool add(Species& s, int direction, int r, int c);

        // prints the current state of the board
        void show(bool isLast);

        bool hop(int& r, int& c, int direction);

        bool left(int& r, int& c, int direction);

        bool right(int& r, int& c, int direction);

        bool infect(int& r, int& c, int direction);

        bool if_empty(int r, int c, int direction);

        bool if_wall(int& r, int& c, int direction);

        bool if_random(int& r, int& c, int direction);

        bool if_enemy(int r, int c, int direction);

        bool go(int& r, int& c, int direction);
};

class Creature {
    private:
        int direction;
        int row;
        int col;
        int programCounter;
        bool turnEnded;

    public:
        Species* mySpecies;

        Creature(Species& s, int dir, int r, int c);

        //start running instructions
        void start(Darwin& d);
        //change direction 90 degrees left
        void turn_left();
        //change direction 90 degrees right
        void turn_right();
        //prints first character of species, calls species.print()
        void print();
        //checks if other is a different species
        bool check_enemy(Creature& other);
        //sets the turnEnded flag
        void set_turn(bool end);
        //sets this creatures programCounter
        void set_pc(int x);
        //checks for ended turn
        bool is_done();
};

class Instruction;

// creature objects will just point to a species type they currently are
class Species {
    private:
        vector<Instruction> iList;
    public:
        char type;

        Species(char t);

        void execute_instructions(Darwin& d, int& r, int& c, int dir, int& pc);

        //adds action instruction
        void add_action(int i);
        //adds control instruction, with go-to line n
        void add_control(int i, int n);
        //prints first character of species type
        void print();
};

class Instruction {
    private:
        int name;

    public:
        // 0 for control vs 1 for action
        int type;
        //go to line (control only)
        int jump;

        Instruction(int i);

        Instruction(int i, int n);

        int getName();
};

int main () {
    Species food('f');
    food.add_action(LEFT);
    food.add_control(GO, 0);

    Species hopper('h');
    hopper.add_action(HOP);
    hopper.add_control(GO, 0);

    Species rover('r');
    rover.add_control(IF_ENEMY,9);
    rover.add_control(IF_EMPTY,7);
    rover.add_control(IF_RANDOM,5);
    rover.add_action(LEFT);
    rover.add_control(GO,0);
    rover.add_action(RIGHT);
    rover.add_control(GO,0);
    rover.add_action(HOP);
    rover.add_control(GO,0);
    rover.add_action(INFECT);
    rover.add_control(GO,0);

    Species trap('t');
    trap.add_control(IF_ENEMY,3);
    trap.add_action(LEFT);
    trap.add_control(GO,0);
    trap.add_action(INFECT);
    trap.add_control(GO,0);

    int testCases;
    cin >> testCases;
    assert(testCases > 0);
    while(testCases-- > 0) {
        srand(0);
        string flush;
        getline(cin, flush);
        int rows;
        int columns;
        cin >> rows;
        cin >> columns;
        //print *** Darwin r x c ***
        cout << "*** Darwin " << rows << "x" << columns << " ***\n";


        int numCreatures;
        cin >> numCreatures;
        Darwin d(rows, columns, numCreatures);
        while(numCreatures-- > 0) {
            char name;
            int r;
            int c;
            char direction;
            cin >> name;
            cin >> r;
            cin >> c;
            cin >> direction;
            int dir;
            switch(direction) {
                case 'n':
                    dir = 0;
                    break;
                case 'e':
                    dir = 1;
                    break;
                case 's':
                    dir = 2;
                    break;
                case 'w':
                    dir = 3;
                    break;
            }
            switch(name) {
                case 'f':
                    d.add(food, dir, r, c);
                    break;
                case 'h':
                    d.add(hopper, dir, r, c);
                    break;
                case 'r':
                    d.add(rover, dir, r, c);
                    break;
                case 't':
                    d.add(trap, dir, r, c);
                    break;
            }
        }
        int turns;
        int freq;
        cin >> turns;
        cin >> freq;
        for(int t = 0; t <= turns; t++) {
            if(t % freq == 0) {
                cout << "Turn = " << t << "." << "\n";
                bool isLast = (testCases == 0) && (t == (turns/freq * freq));
                d.show(isLast);
            }
            if(t != turns)
                d.round();
        }
    }
    return 0;
}


//replace all -1 addresses with nullptr
Darwin::Darwin(int r, int c, int numCreatures) {
    rows = r;
    columns = c;
    creatures.reserve(numCreatures);
    board.resize(r, vector<Creature*>(c, nullptr));
}

// Darwin::~Darwin() {
//     for(int i = 0; i < row; i++) {
//         board[i].clear();
//     }
//     board.clear();
//     creatures.clear();
// }

void Darwin::round() {
    for(int r = 0; r < rows; r++) {
        for(int c = 0; c < columns; c++) {
            //check if address is valid
            if(board[r][c] != nullptr) {
                // board is just addresses? can dereference (i might need to implement the * operator??)
                Creature& current = *board[r][c];
                if(!current.is_done())
                    current.start(*this);
            }
        }
    }
    // make sure to undo the end turn flag on every creature
    for(int i = 0; i < creatures.size(); i++) {
        creatures[i].set_turn(false);
    }
}

//returns whether or not a creature was successfully added
bool Darwin::add(Species& s, int direction, int r, int c) {
        Creature current(s, direction, r, c);
        creatures.push_back(current);
        board[r][c] = &creatures.back(); //should i use &(creatures[creatures.size() - 1]) instead?
        return true;
}

void Darwin::show(bool isLast) {
    cout << "  ";
    for(int c = 0; c < columns; c++)
        cout << c%10;
    cout << "\n";
    cout.flush();
    for(int r = 0; r < rows; r++) {
        cout << r%10 << " ";
        for(int c = 0; c < columns; c++) {
            if(board[r][c] != nullptr){
                board[r][c]->print();
            }
            else
                cout << ".";
        }
        cout << "\n";
    }
    if(!isLast)
        cout << "\n";
}

bool Darwin::hop(int& r, int& c, int direction) {
    assert(board[r][c] != nullptr);
    if(if_empty(r, c, direction)) {
        switch(direction){
            case NORTH:
                board[r-1][c] = board[r][c];
                board[r--][c] = nullptr;
                break;
            case EAST:
                board[r][c+1] = board[r][c];
                board[r][c++] = nullptr;
                break;
            case SOUTH:
                board[r+1][c] = board[r][c];
                board[r++][c] = nullptr;
                break;
            case WEST:
                board[r][c-1] = board[r][c];
                board[r][c--]= nullptr;
                break;
        }
        return true;
    }
    return false;
}

bool Darwin::left(int& r, int& c, int direction) {
    assert(board[r][c] != nullptr);
    //derefrencing creature from address given by board
    (*board[r][c]).turn_left();
    return true;
}

bool Darwin::right(int& r, int& c, int direction) {
    assert(board[r][c] != nullptr);
    //derefrencing creature from address given by board
    (*board[r][c]).turn_right();
    return true;
}

bool Darwin::infect(int& r, int& c, int direction) {
    if(if_enemy(r,c,direction)) {
        //get the creature being infected
        int otherR = r;
        int otherC = c;
        switch(direction) {
            case NORTH:
                otherR--;
                break;
            case EAST:
                otherC++;
                break;
            case SOUTH:
                otherR++;
                break;
            case WEST:
                otherC--;
                break;
        }
        (*board[otherR][otherC]).mySpecies = (*board[r][c]).mySpecies;
        (*board[otherR][otherC]).set_pc(0);
        return true;
    }
    return false;
}

bool Darwin::if_empty(int r, int c, int direction) {
    assert(board[r][c] != nullptr);
    if(!if_wall(r, c, direction)) {
        switch(direction) {
            case NORTH:
                return board[r-1][c] == nullptr;
            case EAST:
                return board[r][c+1] == nullptr;
            case SOUTH:
                return board[r+1][c] == nullptr;
            case WEST:
                return board[r][c-1] == nullptr;
        }
    }
    //wall means not empty
    return false;

}

bool Darwin::if_wall(int& r, int& c, int direction) {
    assert(board[r][c] != nullptr);
    switch(direction) {
        case NORTH:
            return r == 0;
        case EAST:
            return c == columns - 1;
        case SOUTH:
            return r == rows - 1;
        case WEST:
            return c == 0;
    }
    // should be unreachable
    return false;
}

bool Darwin::if_random(int& r, int& c, int direction) {
    //even = fail, odd = success
    if(rand() % 2 == 0)
        return false;
    return true;
}

bool Darwin::if_enemy(int r, int c, int direction) {
    if(board[r][c] == nullptr || if_wall(r,c,direction) || if_empty(r,c,direction))
        return false;
    // now has to be a creature
    int otherR = r;
    int otherC = c;
    switch(direction) {
        case NORTH:
            otherR--;
            break;
        case EAST:
            otherC++;
            break;
        case SOUTH:
            otherR++;
            break;;
        case WEST:
            otherC--;
            break;
    }
    return (*board[r][c]).check_enemy(*board[otherR][otherC]);
}

//always jump to given line
bool Darwin::go(int& r, int& c, int direction) {
    return true;
}

Creature::Creature(Species& s, int dir, int r, int c) {
    mySpecies = &s;
    direction = dir;
    row = r;
    col = c;
    programCounter = 0;
    turnEnded = false;
}

void Creature::start(Darwin& d) {
    mySpecies->execute_instructions(d, row, col, direction, programCounter);
    turnEnded = true;
}

void Creature::turn_left() {
    switch(direction) {
        case NORTH:
            direction = WEST;
            break;
        case EAST:
            direction = NORTH;
            break;
        case SOUTH:
            direction = EAST;
            break;
        case WEST:
            direction = SOUTH;
            break;
    }
}

void Creature::turn_right() {
    switch(direction) {
        case NORTH:
            direction = EAST;
            break;
        case EAST:
            direction = SOUTH;
            break;
        case SOUTH:
            direction = WEST;
            break;
        case WEST:
            direction = NORTH;
            break;
    }
}

void Creature::print() {
    mySpecies->print();
}

bool Creature::check_enemy(Creature& other) {
    //comparing pointers is ok because there is only 1 instance of each species
    return mySpecies != other.mySpecies;
}

void Creature::set_turn(bool end) {
    turnEnded = end;
}

void Creature::set_pc(int x) {
    programCounter = x;
}

bool Creature::is_done() {
    return turnEnded;
}

Species::Species(char t) {
    type = t;
}

void Species::execute_instructions(Darwin& d, int& r, int& c, int dir, int& pc) {
    while(pc < iList.size()) {
        Instruction& current = iList[pc];
        // action = 1 : 1 == true?
        if(current.type) {
            // call darwin's execute instruction
            switch(current.getName()) {
                case HOP:
                    d.hop(r, c, dir);
                    break;
                case LEFT:
                    d.left(r, c, dir);
                    break;
                case RIGHT:
                    d.right(r, c, dir);
                    break;
                case INFECT:
                    d.infect(r, c, dir);
                    break;
            }
            pc++;
            return;
        }
        //control instruction
        else {
            bool success = false;
            switch(current.getName()) {
                case IF_EMPTY:
                    success = d.if_empty(r, c, dir);
                    break;
                case IF_WALL:
                    success = d.if_wall(r, c, dir);
                    break;
                case IF_RANDOM:
                    success = d.if_random(r, c, dir);
                    break;
                case IF_ENEMY:
                    success = d.if_enemy(r, c, dir);
                    break;
                case GO:
                    success = d.go(r, c, dir);
                    break;
            }
            if(success)
                pc = current.jump;
            else
                pc++;
        }
    }
}

void Species::add_action(int i) {
    Instruction current(i);
    iList.push_back(current);
}

void Species::add_control(int i, int n) {
    Instruction current(i, n);
    iList.push_back(current);
}

void Species::print() {
    cout << type;
}

Instruction::Instruction(int i) {
    name = i;
    //1 = action
    type = 1;
    //this should never be used
    jump = -1;
}

Instruction::Instruction(int i, int n) {
    name = i;
    //0 = control
    type = 0;
    jump = n;
}

int Instruction::getName() {
    return name;
}
