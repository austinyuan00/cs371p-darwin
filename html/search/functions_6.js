var searchData=
[
  ['if_5fempty',['if_empty',['../classDarwin.html#a3df11690c398bc976dca7e06561afb15',1,'Darwin::if_empty(int r, int c, int direction)'],['../classDarwin.html#a3df11690c398bc976dca7e06561afb15',1,'Darwin::if_empty(int r, int c, int direction)']]],
  ['if_5fenemy',['if_enemy',['../classDarwin.html#a618272741db602bcb0605ae00942e8ab',1,'Darwin::if_enemy(int r, int c, int direction)'],['../classDarwin.html#a618272741db602bcb0605ae00942e8ab',1,'Darwin::if_enemy(int r, int c, int direction)']]],
  ['if_5frandom',['if_random',['../classDarwin.html#a1e57368500bef2b62cfa8f95bd2a2d1e',1,'Darwin::if_random(int &amp;r, int &amp;c, int direction)'],['../classDarwin.html#a1e57368500bef2b62cfa8f95bd2a2d1e',1,'Darwin::if_random(int &amp;r, int &amp;c, int direction)']]],
  ['if_5fwall',['if_wall',['../classDarwin.html#a7f48fae28440d97e7b14f125034432cf',1,'Darwin::if_wall(int &amp;r, int &amp;c, int direction)'],['../classDarwin.html#a7f48fae28440d97e7b14f125034432cf',1,'Darwin::if_wall(int &amp;r, int &amp;c, int direction)']]],
  ['infect',['infect',['../classDarwin.html#a7250702c3ba12dc53f9862cb6db68686',1,'Darwin::infect(int &amp;r, int &amp;c, int direction)'],['../classDarwin.html#a7250702c3ba12dc53f9862cb6db68686',1,'Darwin::infect(int &amp;r, int &amp;c, int direction)']]],
  ['instruction',['Instruction',['../classInstruction.html#ad8b5f8b9475854edc3108d860f049415',1,'Instruction::Instruction(int i)'],['../classInstruction.html#ab03da4206849b4f6909b4479001ba253',1,'Instruction::Instruction(int i, int n)'],['../classInstruction.html#ad8b5f8b9475854edc3108d860f049415',1,'Instruction::Instruction(int i)'],['../classInstruction.html#ab03da4206849b4f6909b4479001ba253',1,'Instruction::Instruction(int i, int n)']]],
  ['is_5fdone',['is_done',['../classCreature.html#a938f7a74583d9a3df4a400b3b38f6ab8',1,'Creature::is_done()'],['../classCreature.html#a938f7a74583d9a3df4a400b3b38f6ab8',1,'Creature::is_done()']]]
];
