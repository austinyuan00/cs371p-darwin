// ---------
// Species.hpp
// ---------

#ifndef Species_h
#define Species_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <string>   // string
#include <utility>  // pair
#include <vector>    // push_back()?
#include <cassert>

#include "Creature.hpp"
#include "Darwin.hpp"

using namespace std;
class Instruction;
class Darwin;

// creature objects will just point to a species type they currently are
class Species {
private:
    vector<Instruction> iList;
public:
    char type;

    Species(char t);

    void execute_instructions(Darwin& d, int& r, int& c, int dir, int& pc);

    //adds action instruction
    void add_action(int i);
    //adds control instruction, with go-to line n
    void add_control(int i, int n);
    //prints first character of species type
    void print();
};

class Instruction {
private:
    int name;

public:
    // 0 for control vs 1 for action
    int type;
    //go to line (control only)
    int jump;

    Instruction(int i);

    Instruction(int i, int n);

    int getName();
};

#endif //Species_h
